import 'dotenv/config';
import App from './app';
import UserRoute from './routes/user.route';
import PasswordRoute from './routes/password.route';
import LoginRoute from './routes/login.route';
import RoleDataRoute from './routes/roleData.route';
import NotificationRoute from './routes/notification.route';
import LanguageRoute from './routes/language.route';
import JobExperienceRoute from './routes/jobExperience.route';
import JobPostRoute from './routes/jobPost.route';
import IndustryFunctionRoute from './routes/industryFunction.route';
import GroupTitleRoute from './routes/groupTitle.route';
import AppliedJobRoute from './routes/appliedJob.route';

const app = new App([new UserRoute(), new PasswordRoute(),new LoginRoute(), new RoleDataRoute(), new NotificationRoute(), new LanguageRoute(), new JobExperienceRoute(),new JobPostRoute(), new IndustryFunctionRoute(), new GroupTitleRoute, new AppliedJobRoute()]);

app.listen(); 