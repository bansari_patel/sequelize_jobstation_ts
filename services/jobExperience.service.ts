import DB from '../database/database.config';
import { CreateJobExperienceDto } from '../dtos/jobExperience.dto';

class JobExperienceService {

    public jobExperience = DB.JobExperience;
    public async create(jobExperienceData: CreateJobExperienceDto, callback: any ) {
        
        await this.jobExperience.create(jobExperienceData)
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async getDataByCandidateId(candidate_id: number, callback: any ) {
        
        await this.jobExperience.findOne({where: {candidate_id: candidate_id}})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    } 
    
    public async delete(candidate_id: number, callback: any ) {
        
        await this.jobExperience.destroy({
            where: { id: candidate_id }
        })
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }
}

export default JobExperienceService;
