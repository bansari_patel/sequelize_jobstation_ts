import DB from '../database/database.config';
import { CreateCompanyDto } from '../dtos/companyDetails.dto';
import { CreateEducationDto } from '../dtos/educationDetails.dto';
import { CreateLocationDto } from '../dtos/locationDetails.dto';
import { CreateOtherDto } from '../dtos/otherDetails.dto';
import { CreatePersonalDto } from '../dtos/personalDetails.dto';
import { CreateReferenceDto } from '../dtos/referenceDetails.dto';
import { CreateSalaryDto } from '../dtos/salaryDetails.dto';
import { CreateUserDto } from '../dtos/users.dto';
import { CreateDocumentsDto } from '../dtos/upload_documents.dto';

class UserService {

    public users = DB.Users;
    public async create(userData: CreateUserDto, callback: any ) {
        
        await this.users.create(userData)
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    } 
    
    
    public async isUserExist(email_id: string, callback: any){
        await this.users.findOne({where: {email_id: email_id}})
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
        
    }

    public async isUserExistById(user_id: number, callback: any){

        await this.users.findByPk(user_id)
        .then(UserData => {
            if(UserData == null){
                callback(null, null);
            }else{
                callback(null, UserData);
            }
        }).catch(err => {
            callback(err);
        });
        
    }


    public async updateCompany(companyData: CreateCompanyDto,username: string, callback: any ) {
        
        await this.users.update(companyData, {where : {email_id: username}})
        .then(CompanyData => {
            if(CompanyData == null){
                callback(null, null);
            }else{
                callback(null, CompanyData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updatePersonal(personalData: CreatePersonalDto,username: string, callback: any ) {
        
        await this.users.update(personalData, {where : {email_id: username}})
        .then(PersonalData => {
            if(PersonalData == null){
                callback(null, null);
            }else{
                callback(null, PersonalData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updateOther(otherData: CreateOtherDto,username: string, callback: any ) {
        
        await this.users.update(otherData, {where : {email_id: username}})
        .then(OtherData => {
            if(OtherData == null){
                callback(null, null);
            }else{
                callback(null, OtherData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updateEducation(educationData: CreateEducationDto,username: string, callback: any ) {
        
        await this.users.update(educationData, {where : {email_id: username}})
        .then(EducationData => {
            if(EducationData == null){
                callback(null, null);
            }else{
                callback(null, EducationData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updateLocation(locationData: CreateLocationDto, username: string, callback: any ) {
        
        await this.users.update(locationData, {where : {email_id: username}})
        .then(LocationData => {
            if(LocationData == null){
                callback(null, null);
            }else{
                callback(null, LocationData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updateReference(referenceData: CreateReferenceDto, username: string, callback: any ) {
        
        await this.users.update(referenceData, {where : {email_id: username}})
        .then(ReferenceData => {
            if(ReferenceData == null){
                callback(null, null);
            }else{
                callback(null, ReferenceData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }

    public async updateSalary(salaryData: CreateSalaryDto, username: string, callback: any ) {
        
        await this.users.update(salaryData, {where : {email_id: username}})
        .then(SalaryData => {
            if(SalaryData == null){
                callback(null, null);
            }else{
                callback(null, SalaryData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }
    
    public async uploadDocuments(documentsDetails: CreateDocumentsDto,username: string, callback: any ) {
        
        await this.users.update(documentsDetails, {where : {email_id: username}})
        .then(documnetsData => {
            if(documnetsData == null){
                callback(null, null);
            }else{
                callback(null, documnetsData);
            }
        }).catch(err => {
            callback(err);
        }); 
    }
    
}

export default UserService;
