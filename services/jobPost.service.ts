import DB from '../database/database.config';
import { CreateJobPostDto } from '../dtos/jobPost.dto';

class JobPostService {

    public jobPost = DB.JobPost;
    public async create(jobPostData: CreateJobPostDto, callback: any ) {
        
        await this.jobPost.create(jobPostData)
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }
    
}

export default JobPostService; 
