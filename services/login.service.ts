import DB from '../database/database.config';
import { CreateLoginDto } from '../dtos/login.dto';

class LoginService {

    public login = DB.Login;
    public async create(loginData: CreateLoginDto, callback: any) {

        await this.login.create(loginData)
            .then(data => {
                
                callback(null, data);
            }).catch(err => {
                callback(err);
            });
    }

}

export default LoginService; 
