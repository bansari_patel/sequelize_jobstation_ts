import DB from '../database/database.config';
import { CreateAppliedJobDto } from '../dtos/appliedJob.dto';

class AppliedJobService {

    public appliedJob = DB.AppliedJob;
    public async create(appliedJobData: CreateAppliedJobDto, callback: any ) {
        
        await this.appliedJob.create(appliedJobData)
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }
    
}

export default AppliedJobService; 
