import DB from '../database/database.config';

class RoleDataService {

    public roleData = DB.RoleData;

    public async getRoleData(callback: any ) {
        
        await this.roleData.findAll()
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async getRoles(callback: any ) {
        
        await this.roleData.findAll({attributes: ['roles']})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }
} 

export default RoleDataService;
