import DB from '../database/database.config';
import { CreateResetPasswordDto, CreateSetNewPasswordDto } from '../dtos/password.dto';

class PasswordService {

    public users = DB.Users;
    public async resetPassword(resetPasswordData: CreateResetPasswordDto, callback: any ) {
        
        await this.users.update({password: resetPasswordData.password}, {where : {id: resetPasswordData.user_id}})
        .then(ResetPasswordData => {
            if(ResetPasswordData == null){
                callback(null, null);
            }else{
                callback(null, ResetPasswordData);
            }
        }).catch(err => {
            callback(err); 
        });
    } 
    public verifyOTP = (enteredOTP: string, sentOTP: string, callback: any) => {
        if(enteredOTP !== sentOTP) {
          callback(1);
        } else {
          callback(null, 1);
        }
      };
    public async setNewPassword(setNewPasswordData: CreateSetNewPasswordDto, callback: any ) {
        
        await this.users.update({password: setNewPasswordData.password}, {where : {email_id: setNewPasswordData.email_id}})
        .then(SetNewPasswordData => {
            if(SetNewPasswordData == null){
                callback(null, null);
            }else{
                callback(null, SetNewPasswordData);
            }
        }).catch(err => {
            callback(err);
        });
    }    
    
}

export default PasswordService;
