import DB from '../database/database.config';

class IndustryFunctionService {

    public industryFunction = DB.IndustryFunction;

    public async getIndustryFunctionData(callback: any ) {
        
        await this.industryFunction.findAll()
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async getIndustryFunctions(callback: any ) {
        
        await this.industryFunction.findAll({attributes: ['function']})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        }); 
    }
}

export default IndustryFunctionService;
