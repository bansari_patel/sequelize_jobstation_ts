import DB from '../database/database.config';
import { CreateLanguageDto } from '../dtos/languages.dto';

class LanguageService {

    public languages = DB.Language;
    public async create(languageData: CreateLanguageDto, callback: any ) {
        
        await this.languages.create(languageData)
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async getLanguagesByCandidateId(candidate_id: number, callback: any){

        await this.languages.findAll({where: {id: candidate_id}})
        .then(LanguageData => {
            if(LanguageData == null){
                callback(null, null);
            }else{
                callback(null, LanguageData); 
            }
        }).catch(err => {
            callback(err);
        });
        
    }
    
    public async update(languageData: CreateLanguageDto, language_id: number, callback: any ) {
        
        await this.languages.update(languageData, {where : {id: language_id}})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async delete(language_id: number, callback: any ) {
        
        await this.languages.destroy({
            where: { id: language_id }
        })
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }


    
}

export default LanguageService;
