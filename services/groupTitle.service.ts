import DB from '../database/database.config';

class GroupTitleService {

    public groupTitle = DB.GroupTitle;

    public async getGroupTitleData(callback: any ) {
        
        await this.groupTitle.findAll()
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }

    public async getGroupTitles(callback: any ) {
        
        await this.groupTitle.findAll({attributes: ['title']})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        }); 
    }
}

export default GroupTitleService;
