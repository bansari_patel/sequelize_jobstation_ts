import DB from '../database/database.config';

class NotificationService {

    public notification = DB.Notification;

    public async getDataByAppliedId(applied_id: number, callback: any ) {
        
        await this.notification.findOne({where: {applied_id: applied_id}})
            .then(data => {
                callback(null, data);
        }).catch(err => {
            callback(err);
        });
    }
}

export default NotificationService; 
