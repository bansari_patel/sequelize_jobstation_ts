import Joi from 'joi';

const minEmail = 3;
const maxEmail = 320;
const maxPass = 16;
const minPass = 8;
const minMobile = 10; 

export const user = {
    schemaUserRegister: Joi.object({
        name: Joi.string().required().empty().messages({
            'string.base': `name should be a type of 'text'`,
            'string.empty': `name cannot be an empty field`,
            'any.required': `name is a required field`}
        ),
        surname: Joi.string().required().empty().messages({
            'string.base': `surname should be a type of 'text'`,
            'string.empty': `surname cannot be an empty field`,
            'any.required': `surname is a required field`}
        ),
        email_id: Joi.string().min(minEmail).max(maxEmail).required().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.required': `email_id is a required field`}
        ),
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': 'password should be of minimum 8 characters',
            'string.max': 'password should be of maximum 255 characters',
            'any.required': `password is a required field`}
        ),
        mobile: Joi.string().required().min(minMobile).regex(/^\d{10}$/).empty().messages({
            'string.base': `mobile should be a type of 'string'`,
            'string.empty': `mobile cannot be an empty field`,
            'any.required': `mobile is a required field`}
        ),
        gender: Joi.string().required().empty().messages({
            'string.base': `gender should be a type of 'string'`,
            'string.empty': `gender cannot be an empty field`,
            'any.required': `gender is a required field`}
        )}
    ),

    schemaUserCompany: Joi.object({
        current_company: Joi.string().optional().empty().messages({
            'string.base': `current_company should be a type of 'text'`,
            'string.empty': `current_company cannot be an empty field`,
            'any.optional': `current_company is a optional field`}
        ),
        designation: Joi.string().optional().empty().messages({
            'string.base': `designation should be a type of 'text'`,
            'string.empty': `designation cannot be an empty field`,
            'any.optional': `designation is a optional field`}
        ),
        experience: Joi.string().optional().empty().messages({
            'string.base': `experience should be a type of 'text'`,
            'string.empty': `experience cannot be an empty field`,
            'any.optional': `experience is a optional field`}
        ),
        current_salary: Joi.string().optional().empty().messages({
            'string.base': `current_salary should be a type of 'string'`,
            'string.empty': `current_salary cannot be an empty field`,
            'any.optional': `current_salary is a optional field`}
        ),
        expected_salary: Joi.string().optional().empty().messages({
            'string.base': `expected_salary should be a type of 'string'`,
            'string.empty': `expected_salary cannot be an empty field`,
            'any.optional': `expected_salary is a optional field`}
        ),
        notice_period: Joi.string().optional().empty().messages({
            'string.base': `notice_period should be a type of 'string'`,
            'string.empty': `notice_period cannot be an empty field`,
            'any.optional': `notice_period is a optional field`}
        )
    }),
    
    schemaUserPersonal: Joi.object({
        email_id: Joi.string().min(minEmail).max(maxEmail).optional().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.optional': `email_id is a optional field`}
        ),
        name: Joi.string().optional().empty().messages({
            'string.base': `name should be a type of 'text'`,
            'string.empty': `name cannot be an empty field`,
            'any.optional': `name is a optional field`}
        ),
        surname: Joi.string().optional().empty().messages({
            'string.base': `surname should be a type of 'text'`,
            'string.empty': `surname cannot be an empty field`,
            'any.optional': `surname is a optional field`}
        ),
        mobile: Joi.string().optional().min(minMobile).regex(/^\d{10}$/).empty().messages({
            'string.base': `mobile should be a type of 'string'`,
            'string.empty': `mobile cannot be an empty field`,
            'any.optional': `mobile is a optional field`}
        ),
        date_of_birth: Joi.string().optional().empty().messages({
            'string.base': `date_of_birth should be a type of 'text'`,
            'string.empty': `date_of_birth cannot be an empty field`,
            'any.optional': `date_of_birth is a optional field`}
        ),
        education: Joi.string().optional().empty().messages({
            'string.base': `education should be a type of 'string'`,
            'string.empty': `education cannot be an empty field`,
            'any.optional': `education is a optional field`}
        ),
        aadhar_number: Joi.string().optional().regex(/^\d{12}$/).empty().messages({
            'string.base': `aadhar_number should be a type of 'text'`,
            'string.empty': `aadhar_number cannot be an empty field`,
            'any.optional': `aadhar_number is a optional field`}
        )
    }),

    schemaUserOther: Joi.object({
        home_town: Joi.string().optional().empty().messages({
            'string.base': `home_town should be a type of 'text'`,
            'string.empty': `home_town cannot be an empty field`,
            'any.optional': `home_town is a optional field`}
        ),
        current_location: Joi.string().optional().empty().messages({
            'string.base': `current_location should be a type of 'text'`,
            'string.empty': `current_location cannot be an empty field`,
            'any.optional': `current_location is a optional field`}
        ),
        preffered_location: Joi.string().optional().empty().messages({
            'string.base': `preffered_location should be a type of 'text'`,
            'string.empty': `preffered_location cannot be an empty field`,
            'any.optional': `preffered_location is a optional field`}
        ),
        reference_person: Joi.string().optional().empty().messages({
            'string.base': `reference_person should be a type of 'text'`,
            'string.empty': `reference_person cannot be an empty field`,
            'any.optional': `reference_person is a optional field`}
        ),
        apply_post: Joi.string().optional().empty().messages({
            'string.base': `apply_post should be a type of 'string'`,
            'string.empty': `apply_post cannot be an empty field`,
            'any.optional': `apply_post is a optional field`}
        )
    }),

    schemaUserLocation: Joi.object({
        home_town: Joi.string().optional().empty().messages({
            'string.base': `home_town should be a type of 'text'`,
            'string.empty': `home_town cannot be an empty field`,
            'any.optional': `home_town is a optional field`}
        ),
        current_location: Joi.string().optional().empty().messages({
            'string.base': `current_location should be a type of 'text'`,
            'string.empty': `current_location cannot be an empty field`,
            'any.optional': `current_location is a optional field`}
        ),
        preferred_location: Joi.string().optional().empty().messages({
            'string.base': `preferred_location should be a type of 'text'`,
            'string.empty': `preferred_location cannot be an empty field`,
            'any.optional': `preferred_location is a optional field`}
        )
    }),

    schemaUserReference: Joi.object({
        reference_person: Joi.string().optional().empty().messages({
            'string.base': `reference_person should be a type of 'text'`,
            'string.empty': `reference_person cannot be an empty field`,
            'any.optional': `reference_person is a optional field`}
        ),
        reference_contact: Joi.string().optional().empty().messages({
            'string.base': `reference_contact should be a type of 'string'`,
            'string.empty': `reference_contact cannot be an empty field`,
            'any.optional': `reference_contact is a optional field`}
        )
    }),
    
    schemaUserEducation: Joi.object({
        b_course: Joi.string().optional().empty().messages({
            'string.base': `b_course should be a type of 'text'`,
            'string.empty': `b_course cannot be an empty field`,
            'any.optional': `b_course is a optional field`}
        ),
        b_subject: Joi.string().optional().empty().messages({
            'string.base': `b_subject should be a type of 'text'`,
            'string.empty': `b_subject cannot be an empty field`,
            'any.optional': `b_subject is a optional field`}
        ),
        b_passing_year: Joi.string().optional().empty().messages({
            'string.base': `b_passing_year should be a type of 'text'`,
            'string.empty': `b_passing_year cannot be an empty field`,
            'any.optional': `b_passing_year is a optional field`}
        ),
        pg_course: Joi.string().optional().empty().messages({
            'string.base': `pg_course should be a type of 'text'`,
            'string.empty': `pg_course cannot be an empty field`,
            'any.optional': `pg_course is a optional field`}
        ),
        pg_subject: Joi.string().optional().empty().messages({
            'string.base': `pg_subject should be a type of 'text'`,
            'string.empty': `pg_subject cannot be an empty field`,
            'any.optional': `pg_subject is a optional field`}
        ),
        pg_passing_year: Joi.string().optional().empty().messages({
            'string.base': `pg_passing_year should be a type of 'text'`,
            'string.empty': `pg_passing_year cannot be an empty field`,
            'any.optional': `pg_passing_year is a optional field`}
        )
    }),
    
    schemaUserSalary: Joi.object({
        current_salary: Joi.string().optional().empty().messages({
            'string.base': `current_salary should be a type of 'text'`,
            'string.empty': `current_salary cannot be an empty field`,
            'any.optional': `current_salary is a optional field`}
        ),
        expected_salary: Joi.string().optional().empty().messages({
            'string.base': `expected_salary should be a type of 'text'`,
            'string.empty': `expected_salary cannot be an empty field`,
            'any.optional': `expected_salary is a optional field`}
        )
    }),
    
    schemaUserDocuments: Joi.object({
        auth_token: Joi.string().required().empty().messages({
            'string.base': `auth_token should be a type of 'text'`,
            'string.empty': `auth_token cannot be an empty field`,
            'any.required': `auth_token is a required field`}
        ),
        upload_resume: Joi.optional().empty().messages({
            'string.base': `upload_resume should be a type of 'text'`,
            'string.empty': `upload_resume cannot be an empty field`,
            'any.required': `upload_resume is a required field`}
        ),
        upload_photo: Joi.any().optional().empty().messages({
            'string.base': `upload_photo should be a type of 'text'`,
            'string.empty': `upload_photo cannot be an empty field`,
            'any.required': `upload_photo is a required field`}
        )
    })
};

