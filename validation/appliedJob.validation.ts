import Joi from 'joi';

const min = 3;
const max = 128;


export const appliedJob = {
    schemaAppliedJob : Joi.object({
        
        candidate_id: Joi.number().required().empty().messages({
            'string.base': `candidate_id should be a type of 'number'`,
            'string.empty': `candidate_id cannot be an empty field`,
            'any.required': `candidate_id is a required field`}
        ),
        job_id: Joi.number().required().empty().messages({
            'string.base': `job_id should be a type of 'number'`,
            'string.empty': `job_id cannot be an empty field`,
            'any.required': `job_id is a required field`}
        ),
        date: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `date should be a type of 'text'`,
            'string.empty': `date cannot be an empty field`,
            'string.min': `date should be of minimum ${min} characters`,
            'string.max': `date should be of maximum ${max} characters`,
            'any.required': `date is a required field`}
        ) 
    }
)};