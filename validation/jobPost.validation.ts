import Joi from 'joi';

const min = 3;
const max = 128;


export const jobPost = {
    schemaJobPost : Joi.object({
        
        company: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `company should be a type of 'text'`,
            'string.empty': `company cannot be an empty field`,
            'string.min': `company should be of minimum ${min} characters`,
            'string.max': `company should be of maximum ${max} characters`,
            'any.required': `company is a required field`}
        ),
        
        subject: Joi.string().required().empty().messages({
            'string.base': `subject should be a type of 'text'`,
            'string.empty': `subject cannot be an empty field`,
            'any.required': `subject is a required field`}
        ),

        designation: Joi.string().required().empty().messages({
            'string.base': `designation should be a type of 'text'`,
            'string.empty': `designation cannot be an empty field`,
            'any.required': `designation is a required field`}
        ),

        experience: Joi.string().required().empty().messages({
            'string.base': `experience should be a type of 'text'`,
            'string.empty': `experience cannot be an empty field`,
            'any.required': `experience is a required field`}
        ),

        location: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `location should be a type of 'text'`,
            'string.empty': `location cannot be an empty field`,
            'string.min': `location should be of minimum ${min} characters`,
            'string.max': `location should be of maximum ${max} characters`,
            'any.required': `location is a required field`}
        ),

        description: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `description should be a type of 'text'`,
            'string.empty': `description cannot be an empty field`,
            'string.min': `description should be of minimum ${min} characters`,
            'string.max': `description should be of maximum ${max} characters`,
            'any.required': `description is a required field`}
        ),

        time_stamp: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `time_stamp should be a type of 'text'`,
            'string.empty': `time_stamp cannot be an empty field`,
            'string.min': `time_stamp should be of minimum ${min} characters`,
            'string.max': `time_stamp should be of maximum ${max} characters`,
            'any.required': `time_stamp is a required field`}
        ),

        company_url: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `company_url should be a type of 'text'`,
            'string.empty': `company_url cannot be an empty field`,
            'string.min': `company_url should be of minimum ${min} characters`,
            'string.max': `company_url should be of maximum ${max} characters`,
            'any.required': `company_url is a required field`}
        ),

        industry_function: Joi.string().required().empty().messages({
            'string.base': `industry_function should be a type of 'text'`,
            'string.empty': `industry_function cannot be an empty field`,
            'any.required': `industry_function is a required field`}
        ),

        industry_role: Joi.string().required().empty().messages({
            'string.base': `industry_role should be a type of 'text'`,
            'string.empty': `industry_role cannot be an empty field`,
            'any.required': `industry_role is a required field`}
        ),

        industry_name: Joi.string().required().empty().messages({
            'string.base': `industry_name should be a type of 'text'`,
            'string.empty': `industry_name cannot be an empty field`,
            'any.required': `industry_name is a required field`}
        ),
        
        salary: Joi.string().required().empty().messages({
            'string.base': `salary should be a type of 'text'`,
            'string.empty': `salary cannot be an empty field`,
            'any.required': `salary is a required field`}
        ),

        type: Joi.number().required().empty().messages({
            'number.base': `type should be a type of 'number'`,
            'number.empty': `type cannot be an empty field`,
            'any.required': `type is a required field`}
        ),

        b_course: Joi.string().required().empty().messages({
            'string.base': `b_course should be a type of 'text'`,
            'string.empty': `b_course cannot be an empty field`,
            'any.required': `b_course is a required field`}
        ),

        b_subject: Joi.string().required().empty().messages({
            'string.base': `b_subject should be a type of 'text'`,
            'string.empty': `b_subject cannot be an empty field`,
            'any.required': `b_subject is a required field`}
        ),

        pg_course: Joi.string().required().empty().messages({
            'string.base': `pg_course should be a type of 'text'`,
            'string.empty': `pg_course cannot be an empty field`,
            'any.required': `pg_course is a required field`}
        ),

        pg_subject: Joi.string().required().empty().messages({
            'string.base': `pg_subject should be a type of 'text'`,
            'string.empty': `pg_subject cannot be an empty field`,
            'any.required': `pg_subject is a required field`}
        ),

        vacancy: Joi.number().required().empty().messages({
            'number.base': `vacancy should be a type of 'number'`,
            'number.empty': `vacancy cannot be an empty field`,
            'any.required': `vacancy is a required field`}
        ),

        status: Joi.string().required().empty().messages({
            'string.base': `status should be a type of 'text'`,
            'string.empty': `status cannot be an empty field`,
            'any.required': `status is a required field`}
        ),

        position_type: Joi.string().required().empty().messages({
            'string.base': `position_type should be a type of 'text'`,
            'string.empty': `position_type cannot be an empty field`,
            'any.required': `position_type is a required field`}
        )
        
    }
)};