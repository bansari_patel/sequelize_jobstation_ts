import Joi from 'joi';

const maxPass = 16;
const minPass = 8;
const minEmail = 10;
const maxEmail = 320; 

export const password_schema = {
    resetPasswordSchema : Joi.object({
        user_id: Joi.number().required().empty().messages({
            'string.base': `user_id should be a type of 'number'`,
            'string.empty': `user_id cannot be an empty field`,
            'string.email': `user_id format not valid`,
            'any.required': `user_id is a required field`
        }),
        current_password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `current_password should be a type of 'text'`,
            'string.empty': `current_password cannot be an empty field`,
            'string.min': `current_password should be of minimum 8 characters`,
            'string.max': `current_password should be of maximum 16 characters`,
            'any.required': `current_password is a required field`
        }),
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': `password should be of minimum 8 characters`,
            'string.max': `password should be of maximum 16 characters`,
            'any.required': `password is a required field`
        }),
        confirm_password: Joi.string().min(minPass).max(maxPass).regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).required().empty().messages({
            'string.base': `confirm_password should be a type of 'text'`,
            'string.empty': `confirm_password cannot be an empty field`,
            'string.min': `confirm_password should be of minimum 8 characters`,
            'string.max': `confirm_password should be of maximum 16 characters`,
            'any.required': `confirm_password is a required field`
        })
    
    }),

    setNewPasswordSchema : Joi.object({
        user_id: Joi.number().required().empty().messages({
            'string.base': `user_id should be a type of 'number'`,
            'string.empty': `user_id cannot be an empty field`,
            'string.email': `user_id format not valid`,
            'any.required': `user_id is a required field`
        }),
        email_id: Joi.string().min(minEmail).max(maxEmail).required().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.required': `email_id is a required field`
        }),
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': `password should be of minimum 8 characters`,
            'string.max': `password should be of maximum 16 characters`,
            'any.required': `password is a required field`
        }),
        confirm_password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `confirm_password should be a type of 'text'`,
            'string.empty': `confirm_password cannot be an empty field`,
            'string.min': `confirm_password should be of minimum 8 characters`,
            'string.max': `confirm_password should be of maximum 16 characters`,
            'any.required': `confirm_password is a required field`
        })
    
    })

    
};