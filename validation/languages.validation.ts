import Joi from 'joi';

const minLanguage = 3;
const maxLanguage = 320;
const minProf = 8;
const maxProf = 128; 

export const language = {
    schemaLanguagePost: Joi.object({
        language: Joi.string().min(minLanguage).max(maxLanguage).required().empty().messages({
            'string.base': `language should be a type of 'text'`,
            'string.empty': `language cannot be an empty field`,
            'string.email': `language format not valid`,
            'any.required': `language is a required field`
        }
        ),
        proficiency: Joi.string().min(minProf).max(maxProf).required().empty().messages({
            'string.base': `proficiency should be a type of 'text'`,
            'string.empty': `proficiency cannot be an empty field`,
            'string.min': `proficiency should be of minimum 8 characters`,
            'string.max': `proficiency should be of maximum 16 characters`,
            'any.required': `proficiency is a required field`
        }
        ),
        candidate_id: Joi.number().required().empty().messages({
            'string.base': `candidate_id should be a type of 'number'`,
            'string.empty': `candidate_id cannot be an empty field`,
            'string.email': `candidate_id format not valid`,
            'any.required': `candidate_id is a required field`
        }
        )
    }),
    schemaLanguagePut: Joi.object({
        language: Joi.string().min(minLanguage).max(maxLanguage).optional().empty().messages({
            'string.base': `language should be a type of 'text'`,
            'string.empty': `language cannot be an empty field`,
            'string.email': `language format not valid`,
            'any.optional': `language is a optional field`
        }
        ),
        proficiency: Joi.string().min(minProf).max(maxProf).optional().empty().messages({
            'string.base': `proficiency should be a type of 'text'`,
            'string.empty': `proficiency cannot be an empty field`,
            'string.min': `proficiency should be of minimum 8 characters`,
            'string.max': `proficiency should be of maximum 16 characters`,
            'any.optional': `proficiency is a optional field`
        }
        ),
        candidate_id: Joi.number().optional().empty().messages({
            'string.base': `candidate_id should be a type of 'number'`,
            'string.empty': `candidate_id cannot be an empty field`,
            'string.email': `candidate_id format not valid`,
            'any.optional': `candidate_id is a optional field`
        }
        )
    }
    )
};