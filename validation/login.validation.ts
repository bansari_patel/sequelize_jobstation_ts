import Joi from 'joi';

const minEmail = 3;
const maxEmail = 320;
const maxPass = 16;
const minPass = 8;

export const authSchema = {
    schemaLogin : Joi.object({
        username: Joi.string().min(minEmail).max(maxEmail).required().empty().email().messages({
            'string.base': `username should be a type of 'text'`,
            'string.empty': `username cannot be an empty field`,
            'string.email': `username format not valid`,
            'any.required': `username is a required field`}
        ),
        password: Joi.string().min(minPass).max(maxPass).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': `password should be of minimum 8 characters`,
            'string.max': `password should be of maximum 16 characters`,
            'any.required': `password is a required field`}
        )}
)}; 