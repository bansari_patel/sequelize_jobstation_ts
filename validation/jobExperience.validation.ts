import Joi from 'joi';

const min = 3;
const max = 128;


export const job_experience = {
    schemaJobExperience : Joi.object({
        current_company: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `current_company should be a type of 'text'`,
            'string.empty': `current_company cannot be an empty field`,
            'string.email': `current_company format not valid`,
            'any.required': `current_company is a required field`}
        ),
        designation: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `designation should be a type of 'text'`,
            'string.empty': `designation cannot be an empty field`,
            'string.email': `designation format not valid`,
            'any.required': `designation is a required field`}
        ),
        organization: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `organization should be a type of 'text'`,
            'string.empty': `organization cannot be an empty field`,
            'string.min': `organization should be of minimum ${min} characters`,
            'string.max': `organization should be of maximum ${max} characters`,
            'any.required': `organization is a required field`}
        ),
        started_work_from: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `started_work_from should be a type of 'text'`,
            'string.empty': `started_work_from cannot be an empty field`,
            'string.min': `started_work_from should be of minimum ${min} characters`,
            'string.max': `started_work_from should be of maximum 128 characters`,
            'any.required': `started_work_from is a required field`}
        ),
        worked_till: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `worked_till should be a type of 'text'`,
            'string.empty': `worked_till cannot be an empty field`,
            'string.min': `worked_till should be of minimum ${min} characters`,
            'string.max': `worked_till should be of maximum ${max} characters`,
            'any.required': `worked_till is a required field`}
        ),
        your_job_profile: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `your_job_profile should be a type of 'text'`,
            'string.empty': `your_job_profile cannot be an empty field`,
            'string.min': `your_job_profile should be of minimum ${min} characters`,
            'string.max': `your_job_profile should be of maximum ${max} characters`,
            'any.required': `your_job_profile is a required field`}
        ),
        salary: Joi.string().min(min).max(max).required().empty().messages({
            'string.base': `salary should be a type of 'text'`,
            'string.empty': `salary cannot be an empty field`,
            'string.min': `salary should be of minimum ${min} characters`,
            'string.max': `salary should be of maximum ${max} characters`,
            'any.required': `salary is a required field`}
        ),
        candidate_id: Joi.number().required().empty().messages({
            'string.base': `candidate_id should be a type of 'number'`,
            'string.empty': `candidate_id cannot be an empty field`,
            'string.email': `candidate_id format not valid`,
            'any.required': `candidate_id is a required field`}
        )} 
)};