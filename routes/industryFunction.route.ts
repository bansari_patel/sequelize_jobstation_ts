import { Router } from 'express';
import IndustryFunctionController from '../controller/industryFunction.controller';

import { Routes } from '../interface/routes.interface';

class IndustryFunctionRoute implements Routes {
    public path = '/industryFunctions';
    public router = Router();
    public industryFunctionController = new IndustryFunctionController();

    constructor() {
        this.initializeIndustryFunctionRoutes();
    }

    public initializeIndustryFunctionRoutes() {
        this.router.get(`${this.path}`, this.industryFunctionController.getIndustryFunctionData);
        this.router.get(`${this.path}/functions`, this.industryFunctionController.getIndustyFunctions);
    }
} 

export default IndustryFunctionRoute;
