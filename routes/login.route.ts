import { Router } from 'express';
import LoginController from '../controller/login.controller';
import { Routes } from '../interface/routes.interface';
import  {validate} from '../helper/validator.helper.js';
import { authSchema } from '../validation/login.validation';

class LoginRoute implements Routes {
    public path = '/login';
    public router = Router();
    public authController = new LoginController();
    constructor() {
      this.initializeCategoryRoutes();
    }
    
    public initializeCategoryRoutes() {
      this.router.post(`${this.path}`, [ validate.body( authSchema.schemaLogin ) ], this.authController.userLogin);
    }
  }

  
export default LoginRoute; 