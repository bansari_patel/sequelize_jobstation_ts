import { Router } from 'express';
import RoleDataController from '../controller/roleData.controller';

import { Routes } from '../interface/routes.interface';

class RoleDataRoute implements Routes {
    public path = '/roleData';
    public router = Router();
    public roleDataController = new RoleDataController();

    constructor() {
        this.initializeRoleDataRoutes();
    }

    public initializeRoleDataRoutes() {
        this.router.get(`${this.path}`, this.roleDataController.getRoleData);
        this.router.get(`${this.path}/roles`, this.roleDataController.getRoles);
    }
}


export default RoleDataRoute; 
