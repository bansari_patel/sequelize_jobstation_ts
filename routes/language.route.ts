import { Router } from 'express';
import LanguageController from '../controller/languages.controller';
import {authenticate} from '../helper/auth'
import { Routes } from '../interface/routes.interface';
import { validate } from '../helper/validator.helper.js';
import { language } from '../validation/languages.validation';


class LanguageRoute implements Routes {
    public path = '/language';
    public router = Router();
    public languageController = new LanguageController();

    constructor() {
        this.initializeLanguageRoutes();
    }

    public initializeLanguageRoutes() {

        this.router.get(`${this.path}/:candidate_id`, authenticate, this.languageController.getLanguage);
        this.router.post(`${this.path}`, [authenticate, validate.body(language.schemaLanguagePost)], this.languageController.addLanguage);
        this.router.put(`${this.path}/:id`, [authenticate, validate.body(language.schemaLanguagePut)],this.languageController.updateLanguage);
        this.router.delete(`${this.path}/:id`, authenticate, this.languageController.removeLanguage);
    }
} 


export default LanguageRoute;
