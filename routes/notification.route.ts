import { Router } from 'express';
import NotificationController from '../controller/notification.controller';
import { authenticate } from '../helper/auth';

import { Routes } from '../interface/routes.interface';


class NotificationRoute implements Routes {
    public path = '/notification';
    public router = Router();
    public notificationController = new NotificationController();

    constructor() {
        this.initializeNotificationRoutes();
    }

    public initializeNotificationRoutes() {

        this.router.get(`${this.path}/:candidate_id`, authenticate, this.notificationController.getNotificationData);
    }
}


export default NotificationRoute; 
