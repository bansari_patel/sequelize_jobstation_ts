import { Router } from 'express';
import JobPostController from '../controller/jobPost.controller';

import { Routes } from '../interface/routes.interface';
import { validate } from '../helper/validator.helper.js';
import { jobPost } from '../validation/jobPost.validation';
import {authenticate} from '../helper/auth';

class JobPostRoute implements Routes {
    public path = '/jobPost';
    public router = Router();
    public jobPostController = new JobPostController();

    constructor() {
        this.initializeJobPostRoutes();
    }

    public initializeJobPostRoutes() {

        this.router.post(`${this.path}`,[authenticate, validate.body(jobPost.schemaJobPost)], this.jobPostController.postJob);
    }
} 


export default JobPostRoute;
