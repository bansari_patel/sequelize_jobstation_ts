import { Router } from 'express';
import GroupTitleController from '../controller/groupTitle.controller';

import { Routes } from '../interface/routes.interface';

class GroupTitleRoute implements Routes {
    public path = '/groupTitles';
    public router = Router();
    public groupTitleController = new GroupTitleController();

    constructor() {
        this.initializeGroupTitleRoutes();
    }

    public initializeGroupTitleRoutes() {
        this.router.get(`${this.path}`, this.groupTitleController.getGroupTitleData);
        this.router.get(`${this.path}/titles`, this.groupTitleController.getGroupTitles);        
    }
} 

export default GroupTitleRoute;
