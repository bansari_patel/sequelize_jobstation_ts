import { Router } from 'express';
import PasswordController from '../controller/password.controller';
import { password_schema } from '../validation/password.validation';

import { Routes } from '../interface/routes.interface';
import { validate } from '../helper/validator.helper';
import { authenticate } from '../helper/auth';

class PasswordRoute implements Routes {
    public path = '/password';
    public router = Router();
    public passwordController = new PasswordController();

    constructor() {
        this.initializePasswordRoutes();
    }

    public initializePasswordRoutes() {
        this.router.put(`${this.path}/reset`,[ authenticate, validate.body(password_schema.resetPasswordSchema) ], this.passwordController.resetPassword);
        this.router.post(`${this.path}/forget/email`, this.passwordController.sendOTP);
        this.router.post(`${this.path}/forget/verify`, this.passwordController.verifyOTP);
        this.router.put(`${this.path}/forget`,validate.body(password_schema.setNewPasswordSchema), this.passwordController.setNewPassword);
        
    }
} 


export default PasswordRoute;
