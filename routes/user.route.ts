import { Router } from 'express';
import UserController from '../controller/users.controller';
import PasswordController from '../controller/password.controller';
import { Routes } from '../interface/routes.interface';
import  { validate } from '../helper/validator.helper.js';
import {user} from '../validation/user.validation';
import {password_schema} from '../validation/password.validation';
import { validateEmail } from '../helper/validEmail.helper';
import  upload from '../helper/uploadImage.helper';
import {authenticate} from '../helper/auth';


class UserRoute implements Routes {
    public path = '/users';
    public router = Router();
    public userController = new UserController();
    public passwordController = new PasswordController();
    constructor() {
      this.initializeUserRoutes();
    }
    
    public initializeUserRoutes() { 
      
      this.router.get(`${this.path}/:email_id`, authenticate, this.userController.getUserByEmail);
      this.router.post(`${this.path}/register`, [ validateEmail, validate.body( user.schemaUserRegister ) ], this.userController.registerUser);
      this.router.put(`${this.path}/updateCompanyDetails`,[authenticate, validate.body( user.schemaUserCompany )] , this.userController.updateCompanyDetails);
      this.router.put(`${this.path}/updatePersonalDetails`, [authenticate, validate.body( user.schemaUserPersonal )], this.userController.updatePersonalDetails);
      this.router.put(`${this.path}/updateOtherDetails`, [authenticate, validate.body( user.schemaUserOther )], this.userController.updateOtherDetails);
      this.router.put(`${this.path}/updateEducationDetails`, [authenticate, validate.body( user.schemaUserEducation )],this.userController.updateEducationDetails);
      this.router.put(`${this.path}/updateLocationDetails`, [authenticate, validate.body( user.schemaUserLocation )], this.userController.updateLocationDetails);
      this.router.put(`${this.path}/updateReferenceDetails`,[authenticate, validate.body( user.schemaUserReference )], this.userController.updateReferenceDetails);
      this.router.put(`${this.path}/updateSalaryDetails`, [authenticate, validate.body( user.schemaUserSalary )],this.userController.updateReferenceDetails);
      this.router.put(`${this.path}/uploadDocuments`, [ authenticate, upload.fields([{ name: 'upload_resume', maxCount: 1 }, {name: 'upload_photo' , maxCount: 1 }]), validate.body(user.schemaUserDocuments) ], this.userController.uploadDocuments);
    }
  }

export default UserRoute;
