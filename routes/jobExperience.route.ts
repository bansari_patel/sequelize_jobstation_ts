import { Router } from 'express';
import JobExperienceController from '../controller/jobExperience.controller';
import {authenticate} from '../helper/auth';
import { Routes } from '../interface/routes.interface';
import { validate } from '../helper/validator.helper.js';
import { job_experience } from '../validation/jobExperience.validation';


class JobExperienceRoute implements Routes {
    public path = '/jobExperience';
    public router = Router();
    public jobExperienceController = new JobExperienceController();

    constructor() {
        this.initializeJobExperienceRoutes();
    }

    public initializeJobExperienceRoutes() {

        this.router.get(`${this.path}/:candidate_id`, authenticate ,this.jobExperienceController.getExperienceData);
        this.router.post(`${this.path}`, [authenticate, validate.body(job_experience.schemaJobExperience)], this.jobExperienceController.addExperienceData);
        this.router.delete(`${this.path}/:candidate_id`, authenticate, this.jobExperienceController.removeJobExperienceData);
    }
} 


export default JobExperienceRoute;
