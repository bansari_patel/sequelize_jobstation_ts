import { Router } from 'express';
import AppliedJobController from '../controller/appliedJob.controller';

import { Routes } from '../interface/routes.interface';
import { validate } from '../helper/validator.helper.js';
import { appliedJob } from '../validation/appliedJob.validation';


class AppliedJobRoute implements Routes {
    public path = '/appliedJob';
    public router = Router();
    public appliedJobController = new AppliedJobController();

    constructor() {
        this.initializeAppliedJobRoutes();
    }

    public initializeAppliedJobRoutes() {

        this.router.post(`${this.path}`, validate.body(appliedJob.schemaAppliedJob), this.appliedJobController.addAppliedJob);
    }
} 


export default AppliedJobRoute;
