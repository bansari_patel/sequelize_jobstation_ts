import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Language } from '../interface/languages.interface';

export type LanguageAttributes = Optional<Language, 'id' | 'language' | 'proficiency' | 'candidate_id' >;

export class LanguageModel extends Model<Language, LanguageAttributes> implements Language {
    public id!: number;
    public language !: string;
    public proficiency !: string;
    public candidate_id !: number;
  }
 
export default function (sequelize: Sequelize): typeof LanguageModel {
  LanguageModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER(),
      },
      language: {
        allowNull: false,
        type: DataTypes.STRING(320),
      },
      proficiency: {
        allowNull: false,
        type: DataTypes.STRING(255),
      },
      candidate_id : {
        allowNull: false,
        type: DataTypes.INTEGER,
      }
    },
    {
      tableName: 'language_known',
      sequelize,
    },
  );

  return LanguageModel;
}
  