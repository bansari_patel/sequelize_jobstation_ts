import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Role } from '../interface/roleData.interface';

export type RoleAttributes = Optional<Role, 'id' | 'roles' | 'group_title_id' >;

export class RoleDataModel extends Model<Role, RoleAttributes> implements Role {
    public id!: number;
    public roles !: string;
    public group_title_id!: number;
  }
 
export default function (sequelize: Sequelize): typeof RoleDataModel {
  RoleDataModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      roles: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      group_title_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      }
    },
    {
      tableName: 'roledata',
      sequelize,
    },
  );

  return RoleDataModel;
}
  