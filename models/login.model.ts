import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Login } from '../interface/login.interface';

export type LoginAttributes = Optional<Login, 'id' | 'username' | 'password' | 'device_token' >;

export class LoginModel extends Model<Login, LoginAttributes> implements Login {
    public id!: number;
    public username !: string;
    public password !: string;
    public device_token !: string;

  }
 
export default function (sequelize: Sequelize): typeof LoginModel {
  LoginModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      username: {
        allowNull: false,
        type: DataTypes.STRING(320),
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING(255),
      },
      device_token : {
        allowNull: true,
        type: DataTypes.STRING,
      },
    },
    {
      tableName: 'login',
      sequelize,
    },
  );

  return LoginModel;
}
  