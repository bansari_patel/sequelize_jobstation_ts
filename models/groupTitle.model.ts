import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { GroupTitle } from '../interface/groupTitle.interface';

export type GroupTitleAttributes = Optional<GroupTitle, 'id' | 'group_id' | 'title' >;

export class GroupTitleModel extends Model<GroupTitle, GroupTitleAttributes> implements GroupTitle {
    public id!: number;
    public group_id!: number;
    public title !: string;
  }
 
export default function (sequelize: Sequelize): typeof GroupTitleModel {
  GroupTitleModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      group_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING(256),
      }
    },
    {
      tableName: 'group_titles',
      sequelize,
    },
  );

  return GroupTitleModel;
}
  