import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { JobExperience } from '../interface/jobExperience.interface';

export type JobExperienceAttributes = Optional<JobExperience, 'id' | 'designation' | 'organization'| 'started_work_from' | 'worked_till' | 'your_job_profile' | 'salary' | 'candidate_id'>;

export class JobExperienceModel extends Model<JobExperience, JobExperienceAttributes> implements JobExperience {
    public id!: number;
    public designation!: string;
    public organization!: string;
    public started_work_from!: string;
    public worked_till!: string;
    public your_job_profile!: string;
    public salary!: string;
    public candidate_id!: number;

  }
 
export default function (sequelize: Sequelize): typeof JobExperienceModel {
  JobExperienceModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER(),
      },
      designation: {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      organization: {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      started_work_from : {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      worked_till : {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      your_job_profile: {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      salary: {
        allowNull: false,
        type: DataTypes.STRING(),
      },
      candidate_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      }
    },
    {
      tableName: 'job_experience',
      sequelize,
    },
  );

  return JobExperienceModel;
}
  