import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { User } from '../interface/users.interface';

export type UserCreationAttributes = Optional<User, 'id' | 'email_id' | 'password' | 'name' | 'surname' | 'mobile' | 'gender' | 'current_company' |'education' | 'designation' | 'experience' | 'current_salary' | 'expected_salary' | 'notice_period' | 'date_of_birth' | 'current_location' | 'home_town' | 'preferred_location'| 'reference_person'|  'reference_contact' | 'apply_post' | 'upload_resume' | 'upload_photo' | 'b_course' | 'b_subject' | 'b_passing_year' | 'pg_course' | 'pg_subject' | 'pg_passing_year' | 'device_token' | 'started_work_from' | 'job_description' | 'industry_function' | 'industry_role' | 'industry_name' | 'aadhar_no' | 'register_date' >;
 
export class UserModel extends Model<User, UserCreationAttributes> implements User {
    public id!: number;
    public mobile!: string;
    public name!: string;
    public surname!: string;
    public email_id !: string;
    public gender !: string;
    public current_company !: string;
    public education !: string;
    public designation !: string;
    public experience !: string;
    public current_salary !: string;
    public expected_salary !: string;
    public notice_period !: string;
    public date_of_birth !: string;
    public current_location !: string;
    public home_town !: string;
    public preferred_location !: string;
    public reference_person !: string;
    public reference_contact!: string;
    public apply_post !: string;
    public upload_resume !: string;
    public upload_photo !: string;
    public b_course!: string;
    public b_subject!: string;
    public b_passing_year!: string;
    public pg_course!: string;
    public pg_subject!: string;
    public pg_passing_year!: string;
    public device_token!: string;
    public started_work_from!: string;
    public job_description!: string;
    public industry_function!: string;
    public industry_role!: string;
    public industry_name!: string;
    public password!: string;
    public aadhar_no!: string;
    public register_date!: Date;

  }

export default function (sequelize: Sequelize): typeof UserModel {
  UserModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      mobile: {
        allowNull: false,
        type: DataTypes.STRING(13),
      },
      name: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      surname: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      email_id: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING(320),
      },
      gender: {
        type: DataTypes.STRING,
      },
      current_company: {
        type: DataTypes.STRING,
      },
      education: {
        type: DataTypes.STRING,
      },
      designation: {
        type: DataTypes.STRING,
      },
      experience: {
        type: DataTypes.STRING,
      },
      current_salary: {
        type: DataTypes.STRING,
      },
      expected_salary: {
        type: DataTypes.STRING,
      },
      notice_period: {
        type: DataTypes.STRING,
      },
      date_of_birth: {
        type: DataTypes.STRING,
      },
      current_location: {
        type: DataTypes.STRING,
      },
      home_town: {
        type: DataTypes.STRING,
      },
      preferred_location: {
        type: DataTypes.STRING,
      },
      reference_person: {
        type: DataTypes.STRING,
      },
      reference_contact: {
        type: DataTypes.STRING,
      },
      apply_post: {
        type: DataTypes.STRING,
      },
      upload_resume: {
        type: DataTypes.STRING,
      },
      upload_photo: {
        type: DataTypes.STRING,
      },
      b_course: {
        type: DataTypes.STRING,
      },
      b_subject: {
        type: DataTypes.STRING,
      },
      b_passing_year: {
        type: DataTypes.STRING,
      },
      pg_course: {
        type: DataTypes.STRING,
      },
      pg_subject: {
        type: DataTypes.STRING,
      },
      pg_passing_year: {
        type: DataTypes.STRING,
      },
      device_token: {
        type: DataTypes.STRING,
      },
      started_work_from: {
        type: DataTypes.STRING,
      },
      job_description: {
        type: DataTypes.STRING,
      },
      industry_function: {
        type: DataTypes.STRING,
      },
      industry_role: {
        type: DataTypes.STRING,
      },
      industry_name: {
        type: DataTypes.STRING,
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING(255),
      },
      aadhar_no: {
        type: DataTypes.STRING,
      },
      register_date: {
        type: DataTypes.DATE,
      },
    },
    {
      tableName: 'candidate_details',
      sequelize,
    },
  );

  return UserModel;
}
  