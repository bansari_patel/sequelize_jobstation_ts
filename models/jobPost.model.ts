import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { JobPost } from '../interface/jobPost.interface';

export type JobPostAttributes = Optional<JobPost, 'id' | 'company' | 'subject' | 'designation' | 'experience' |
 'location' | 'description' | 'time_stamp' | 'company_url' | 'industry_function' | 'industry_role' | 
 'industry_name' | 'salary' | 'type' | 'b_course' | 'b_subject' |'pg_course' | 'pg_subject' | 'vacancy' |
'status' | 'position_type' >;
 
export class JobPostModel extends Model<JobPost, JobPostAttributes> implements JobPost {
    public id!: number;
    public company!: string;
    public subject!: string;
    public designation!: string;
    public experience!: string;
    public location!: string;
    public description!: string;
    public time_stamp!: Date;
    public company_url!: string;
    public industry_function!: string;
    public industry_role!: string;
    public industry_name!: string;
    public salary!: string;
    public type!: number;
    public b_course!: string;
    public b_subject!: string;
    public pg_course!: string;
    public pg_subject!: string;
    public vacancy!: number;
    public status!: string;
    public position_type!: string;
  }

export default function (sequelize: Sequelize): typeof JobPostModel {
  JobPostModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER(),
      },
      company: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      subject: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      designation: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      experience: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      location: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      time_stamp: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      company_url: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      industry_function: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      industry_role: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      industry_name: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      salary: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      type: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      b_course: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      b_subject: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      pg_course: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      pg_subject: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      vacancy: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.STRING(256),
      },
      position_type: {
        allowNull: false,
        type: DataTypes.STRING(256),
      }
    },
    {
      tableName: 'job_post',
      sequelize,
    },
  );

  return JobPostModel;
}
  