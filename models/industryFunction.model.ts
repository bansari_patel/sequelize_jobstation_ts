import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { IndustryFunction } from '../interface/industryFunction.interface';

export type IndustryFunctionAttributes = Optional<IndustryFunction, 'id' | 'function'>;

export class IndustryFunctionModel extends Model<IndustryFunction, IndustryFunctionAttributes> implements IndustryFunction {
    public id!: number;
    public function!: string;
  }
 
export default function (sequelize: Sequelize): typeof IndustryFunctionModel {
  IndustryFunctionModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      function: {
        allowNull: false,
        type: DataTypes.STRING(256),
      }
    },
    {
      tableName: 'industry_function',
      sequelize,
    },
  );

  return IndustryFunctionModel;
}
  