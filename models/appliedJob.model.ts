import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { AppliedJob } from '../interface/appliedJob.interface';

export type AppliedJobAttributes = Optional<AppliedJob, 'id' | 'candidate_id' | 'job_id'| 'date' >;

export class AppliedJobModel extends Model<AppliedJob, AppliedJobAttributes> implements AppliedJob {
    public id!: number;
    public candidate_id !: number;
    public job_id !: number;
    public date !: string;

  }
 
export default function (sequelize: Sequelize): typeof AppliedJobModel {
  AppliedJobModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER(),
      },
      candidate_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      job_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      date : {
        allowNull: false,
        type: DataTypes.STRING(),
      },
    },
    {
      tableName: 'applied_job',
      sequelize,
    },
  );

  return AppliedJobModel;
}
  