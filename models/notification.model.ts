import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Notification } from '../interface/notification.interface';

export type NotificationAttributes = Optional<Notification, 'id' | 'applied_id' | 'time' | 'date' | 'description' >;

export class NotificationModel extends Model<Notification, NotificationAttributes> implements Notification {
    public id!: number;
    public applied_id!: number;
    public time!: string;  
    public date!: string;
    public description!: string;
  }
 
export default function (sequelize: Sequelize): typeof NotificationModel {
  NotificationModel.init(
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER(),
      },
      applied_id: {
        allowNull: false,
        type: DataTypes.INTEGER(),
      },
      time: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      date : {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      description : {
        allowNull: false,
        type: DataTypes.STRING(256),
      }
    },
    {
      tableName: 'notification',
      sequelize,
    },
  );

  return NotificationModel;
}
  