import { IsString, IsEmail } from 'class-validator';

export class CreatePersonalDto {
    @IsEmail()
    public email_id: string | undefined;
    @IsString()
    public name: string | undefined;
    @IsString()
    public surname: string | undefined;
    @IsString()
    public mobile: string | undefined;
    @IsString()
    public date_of_birth: string | undefined;
    @IsString()
    public education: string | undefined;
    @IsString()
    public aadhar_number: string | undefined;
} 