import { IsString } from 'class-validator';

export class CreateOtherDto {
    @IsString()
    public home_town: string | undefined;
    @IsString()
    public current_location: string | undefined;
    @IsString()
    public preferred_location: string | undefined;
    @IsString()
    public reference_person: string | undefined;
    @IsString()
    public apply_post: string | undefined;
} 