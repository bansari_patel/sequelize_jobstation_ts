import { IsString } from 'class-validator';

export class CreateLocationDto {
    @IsString()
    public home_town: string | undefined;
    @IsString()
    public current_location: string | undefined;
    @IsString()
    public preferred_location: string | undefined;
} 