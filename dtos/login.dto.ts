import { IsString ,IsEmail } from 'class-validator';

export class CreateLoginDto {
    @IsEmail()
    public username: string | undefined;
    @IsString()
    public password: string | undefined;
    @IsString()
    public device_token: string | undefined;
} 