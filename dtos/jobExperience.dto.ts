import { IsString, IsNumber} from 'class-validator';

export class CreateJobExperienceDto {
    @IsString()
    public current_company: string | undefined;
    @IsString()
    public designation: string | undefined;
    @IsString()
    public organization: string | undefined;
    @IsString()
    public started_work_from: string | undefined;
    @IsString()
    public worked_till: string | undefined;
    @IsString()
    public your_job_profile: string | undefined;
    @IsString()
    public salary: string | undefined;
    @IsNumber()
    public candidate_id: number | undefined;
} 