import { IsString } from 'class-validator';

export class CreateReferenceDto {
    @IsString()
    public reference_person: string | undefined;
    @IsString()
    public reference_contact: string | undefined;
} 