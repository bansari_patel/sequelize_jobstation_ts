import { IsString, IsNumber, IsDateString, IsDate } from 'class-validator';

export class CreateNotificationDto {
    @IsNumber()
    public applied_id: number | undefined;
    @IsString()
    public time: string | undefined;
    @IsString()
    public date: string | undefined;
    @IsString()
    public description: string | undefined;

} 