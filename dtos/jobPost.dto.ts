import { IsString ,IsDate, IsNumber } from 'class-validator';

export class CreateJobPostDto {

    @IsString()
    public company: string | undefined;
    @IsString()
    public subject: string | undefined;
    @IsString()
    public designation: string | undefined;
    @IsString()
    public experience: string | undefined;
    @IsString()
    public location: string | undefined;
    @IsString()
    public description: string | undefined;
    @IsDate()
    public time_stamp: Date | undefined;
    @IsString()
    public company_url: string | undefined; 
    @IsString()
    public industry_function: string | undefined;
    @IsString()
    public industry_role: string | undefined;
    @IsString()
    public industry_name: string | undefined;
    @IsString()
    public salary: string | undefined;
    @IsNumber()
    public type: number | undefined;
    @IsString()
    public b_course: string | undefined;
    @IsString()
    public b_subject: string | undefined;
    @IsString()
    public pg_course: string | undefined;
    @IsString()
    public pg_subject: string | undefined;
    @IsNumber()
    public vacancy: number | undefined;
    @IsString()
    public status: string | undefined;
    @IsString()
    public position_type: string | undefined;
}