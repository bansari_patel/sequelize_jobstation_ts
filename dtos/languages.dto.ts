import { IsString, IsNumber, IsDateString, IsDate } from 'class-validator';

export class CreateLanguageDto {
    @IsString()
    public language: string | undefined;
    @IsString()
    public proficiency: string | undefined;
    @IsNumber()
    public candidate_id: number | undefined;
} 