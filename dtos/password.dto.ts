import { IsString ,IsNumber, IsEmail } from 'class-validator';

export class CreateResetPasswordDto {
    @IsNumber()
    public user_id!: number;
    @IsString()
    public current_password!: string ;
    @IsString()
    public password!: string ;
    @IsString()
    public confirm_password!: string;
}

export class CreateSetNewPasswordDto {
    @IsNumber()
    public user_id!: number;
    @IsEmail()
    public email_id!: string;
    @IsString()
    public password!: string;
    @IsString()
    public confirm_password!: string;
} 