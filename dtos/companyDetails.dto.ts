import { IsString } from 'class-validator';

export class CreateCompanyDto {
    @IsString()
    public current_company: string | undefined;
    @IsString()
    public designation: string | undefined;
    @IsString()
    public experience: string | undefined;
    @IsString()
    public current_salary: string | undefined;
    @IsString()
    public expected_salary: string | undefined;
    @IsString()
    public notice_period: string | undefined;
} 