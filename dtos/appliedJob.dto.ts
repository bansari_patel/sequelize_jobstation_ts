import { IsNumber, IsString } from 'class-validator';

export class CreateAppliedJobDto {
   
    @IsNumber()
    public candidate_id: number | undefined;
    @IsNumber()
    public job_id: number | undefined;
    @IsString()
    public date: string | undefined;
} 