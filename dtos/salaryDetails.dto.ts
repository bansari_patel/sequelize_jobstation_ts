import { IsString } from 'class-validator';

export class CreateSalaryDto {
    @IsString()
    public current_salary: string | undefined;
    @IsString()
    public expected_salary: string | undefined;
} 