import { IsString } from 'class-validator';

export class CreateEducationDto {
    @IsString()
    public b_course: string | undefined;
    @IsString()
    public b_subject: string | undefined;
    @IsString()
    public pg_course: string | undefined;
    @IsString()
    public pg_subject: string | undefined;
    @IsString()
    public b_passing_year: string | undefined;
    @IsString()
    public pg_passing_year: string | undefined;
} 