import { IsString } from 'class-validator';

export class CreateDocumentsDto {
    @IsString()
    public upload_resume!: string | undefined;
    @IsString()
    public upload_photo!: string | undefined;
} 