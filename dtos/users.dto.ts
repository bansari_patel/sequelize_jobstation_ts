import { IsString ,IsEmail, IsDate } from 'class-validator';

export class CreateUserDto {
    @IsString()
    public mobile!: string;
    @IsEmail()
    public email_id!: string;
    @IsString()
    public name!: string;
    @IsString()
    public surname!: string;
    @IsString()
    public current_company!: string;
    @IsString()
    public designation!: string;
    @IsString()
    public experience!: string;
    @IsString()
    public current_salary!: string;
    @IsString()
    public expected_salary!: string;
    @IsString()
    public notice_period!: string;
    @IsString()
    public date_of_birth!: string;
    @IsString()
    public current_location!: string;
    @IsString()
    public home_town!: string; 
    @IsString()
    public preferred_location!: string;
    @IsString()
    public reference_person!: string;
    @IsString()
    public reference_contact!: string;
    @IsString()
    public apply_post!: string;
    @IsString()
    public upload_resume!: string;
    @IsString()
    public upload_photo!: string;
    @IsString()
    public gender!: string;
    @IsString()
    public b_course!: string;
    @IsString()
    public b_subject!: string;
    @IsString()
    public b_passing_year!: string;
    @IsString()
    public pg_course!: string;
    @IsString()
    public pg_subject!: string;
    @IsString()
    public pg_passing_year!: string;
    @IsString()
    public device_token!: string;
    @IsString()
    public started_work_from!: string;
    @IsString()
    public job_description!: string;
    @IsString()
    public industry_function!: string;
    @IsString()
    public industry_role!: string;
    @IsString()
    public industry_name!: string;
    @IsString()
    public password!: string;
    @IsString()
    public aadhar_no!: string;
    @IsDate()
    public register_date!: Date;
}