import jwt from 'jsonwebtoken';
import config from 'config';
import error from '../utils/error';
import { NextFunction } from 'express';

export const authenticate = (req: any, res: any, next: NextFunction) => {
    const { jwtPrivateKey } = config.get('jwt');

    try{
        const token = req.headers['auth_token'];
        
        if(token !== undefined){
        
            jwt.verify(token, jwtPrivateKey, (err: any, decoded: any) => {
                if (err) {
                  next(new error.UnAuthorized('auth token is invalid'));
                } else {
                  req.decoded = decoded;
                  next();
                }
              });
        }else{
            next(new error.UnAuthorized('auth token not supplied'));
        }
    }catch(ex){
        next(new error.UnAuthorized('authorization error'));
    }
} 
