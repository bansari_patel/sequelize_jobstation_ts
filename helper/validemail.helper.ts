import UserService from '../services/users.service';
import { NextFunction, Request, Response } from 'express';
import error from '../utils/error.js';

export const validateEmail = (req: Request, res: Response, next: NextFunction) => {
  const email = req.body.email_id;
  if (email === undefined) {
    next(new error.BadRequest('email required'));
  } else {
    new UserService().isUserExist(email, (err: any, response: any) => {
      if (response != null) {
        next(new error.BadRequest('User already exist.'));
      } else if (err) {
        next(new error.GeneralError('Error while checking for user'));
      } else {
        next();
      }
    });
  }
};
 