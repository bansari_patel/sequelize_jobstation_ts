
import DB from '../database/database.config';
const users = DB.Users;

export const isValidMail = async function isValid(email : string, callback: any) {
    await users.findOne({where: {email_id: email}})
        .then(UserData => {
            
            if(UserData == null){
                callback(1, null);
            }else{
                callback(0, 1);
            }
        }).catch(err => {
            callback(err);
        });
  }; 