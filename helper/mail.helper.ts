import sendgrid from '@sendgrid/mail';
import config from 'config';

const { SENDGRID_API_KEY , sender_email } = config.get('mail');

sendgrid.setApiKey(SENDGRID_API_KEY);

export function sendConfirmationMail(user: any, callback: any){
    
    const msg = {
        to: user.email_id, 
        from: sender_email,
        subject: 'Sending Email using sendgrid',
        text: 'test mail',
        html: `<h1>Welcome</h1><p>hello<strong> ${user.name} ${user.surname}</strong>, you have successfully registered.</p>`,
      }
    sendgrid
        .send(msg)
        .then((resp) => {
            callback(null, resp);
        })
        .catch((error) => {
            callback(error);
        }) 
}

export function sendOTPMail(email_id: string, OTP: string, callback:any){
    const msg = {
        to: email_id, 
        from: sender_email,
        subject: 'Sending OTP via email using sendgrid',
        text: 'OTP mail',
        html: `<h1>Welcome</h1><p>Hello, Your OTP is  ${OTP}</p>`,
      }
    sendgrid
        .send(msg)
        .then((resp) => {
            callback(null, resp);
        })
        .catch((error) => {
            callback(error,null)
        })
}