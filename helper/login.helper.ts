import jwt from 'jsonwebtoken';
import {User} from '../interface/users.interface'
import config from 'config';

const { jwtPrivateKey , TOKEN_EXPIRY } = config.get('jwt');


export const generateAuthToken = (user: User) => {
    return jwt.sign({ id : user.id,email_id: user.email_id},jwtPrivateKey,{expiresIn: TOKEN_EXPIRY});
}; 
