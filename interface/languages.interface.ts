export interface Language {
    id: number;
    language: string;
    proficiency: string;  
    candidate_id: number;
  
} 