export interface JobPost {
    id: number;
    company: string;
    subject: string;
    designation: string;
    experience: string;
    location: string;
    description: string;
    time_stamp: Date;
    company_url: string;
    industry_function: string;
    industry_role: string;
    industry_name: string;
    salary: string;
    type: number;
    b_course: string;
    b_subject: string;
    pg_course: string;
    pg_subject: string;
    vacancy: number;
    status: string;
    position_type: string;
} 