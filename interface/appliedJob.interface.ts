export interface AppliedJob {
    id: number;
    candidate_id: number;
    job_id: number;
    date: string
} 