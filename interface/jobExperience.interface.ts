export interface JobExperience {
    id: number;
    designation: string;
    organization: string;
    started_work_from: string;
    worked_till: string;
    your_job_profile: string;
    salary: string;
    candidate_id: number;
} 