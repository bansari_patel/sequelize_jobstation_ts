export interface Login {
    id: number;
    username: string;
    password: string;  
    device_token: string;
} 