export interface GroupTitle {
    id: number;
    group_id: number;
    title: string;
} 