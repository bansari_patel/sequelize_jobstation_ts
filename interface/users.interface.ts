export interface User {
    id: number;
    mobile: string;
    email_id: string;
    name: string;
    surname: string;
    gender: string;
    current_company : string;
    education : string;
    designation : string;
    experience : string;
    current_salary : string;
    expected_salary : string;
    notice_period : string; 
    date_of_birth : string;
    current_location : string;
    home_town : string;
    preferred_location : string;
    reference_person : string;
    reference_contact: string;
    apply_post : string;
    upload_resume : string;
    upload_photo : string;
    b_course: string;
    b_subject: string;
    b_passing_year: string;
    pg_course: string;
    pg_subject: string;
    pg_passing_year: string;
    device_token: string;
    started_work_from: string;
    job_description: string;
    industry_function: string;
    industry_role: string;
    industry_name: string;
    password: string;
    aadhar_no: string;
    register_date: Date;
}