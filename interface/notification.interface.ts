export interface Notification {
    id: number;
    applied_id: number;
    time: string;  
    date: string;
    description: string;
} 