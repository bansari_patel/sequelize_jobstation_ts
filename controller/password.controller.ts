import { NextFunction, Request, Response } from 'express';
import UserService from '../services/users.service';
import PasswordService from '../services/password.service';
import { GeneralResponse } from '../utils/response.js';
import bcrypt from 'bcrypt';
import error from '../utils/error';
import config from '../utils/config';
import { CreateResetPasswordDto, CreateSetNewPasswordDto } from '../dtos/password.dto';
import { getPassword } from '../helper/getPassword.helper';
import { isUserValid } from '../helper/validUser.helper';
import { generateOTP } from '../helper/OTP.generator.helper';
import { isValidMail } from '../helper/isValidEmail.helper';
import { sendOTPMail } from '../helper/mail.helper';


const hashRound = 10;

class PasswordController {
    private OTP: number = generateOTP();
    public userService = new UserService();
    public passwordService = new PasswordService();

    public resetPassword = async (req: Request, res: Response, next: NextFunction) => {
        try {
                const resetPasswordData: CreateResetPasswordDto = req.body;
                const originalPassword: any = await getPassword(resetPasswordData.user_id);

                const flag: boolean = await bcrypt.compare(resetPasswordData.current_password, originalPassword);
                if (flag && resetPasswordData.password == resetPasswordData.confirm_password) {
                    resetPasswordData.password = await bcrypt.hash(resetPasswordData.confirm_password, hashRound);

                    this.passwordService.resetPassword(resetPasswordData, (err: any, response: any) => {
                        if (err) {
                            next(new error.GeneralError('Setting new password failed'));
                        } else if (response == 1) {
                            next(
                                new GeneralResponse('Password resetting successful', undefined, config.SUCCESS)
                            );
                        } else {
                            next(new error.GeneralError('Resetting password failed'));
                        }
                    });

                }
                else {
                    next(new error.UnAuthorized('Details are not entered correctly.'));
                }

        } catch (err) {
            next(new error.GeneralError('error while resetting password'));
        }
    }

    public sendOTP = async (req: Request, res: Response, next: NextFunction) => {
        const email = req.body.email;
        
        await isValidMail(email, (Error: any, result: any) => {

            if (result) {
                sendOTPMail(email, this.OTP.toString(), (err: any, response: any) => {
                    if (err && !response) {
                        next(new error.GeneralError('Sending email failed'));
                    } else {
                        next(
                            new GeneralResponse(
                                'Verification Email successfully sent',
                                undefined,
                                config.SUCCESS
                            )
                        );
                    }
                }
                );
            } else {
                next(new error.UnAuthorized('enter registered email'));
            }
        });
    };


    public verifyOTP = (req: Request, res: Response, next: NextFunction) => {
        const enteredOTP = req.body.OTP;

        this.passwordService.verifyOTP(enteredOTP, this.OTP.toString(), (err: any, response: any) => {
            if (err && !response) {
                next(new error.GeneralError('OTP is invalid'));
            } else {
                next(
                    new GeneralResponse(
                        'Your account is verified. Set new password',
                        undefined,
                        config.SUCCESS
                    )
                );
            }
        });
    };


    public setNewPassword = async (req: Request, res: Response, next: NextFunction) => {

        const setNewPasswordData: CreateSetNewPasswordDto = req.body;

        if (await isUserValid(setNewPasswordData.user_id, setNewPasswordData.email_id) && setNewPasswordData.password === setNewPasswordData.confirm_password) {
            setNewPasswordData.password = await bcrypt.hash(setNewPasswordData.confirm_password, hashRound);

            try {

                this.passwordService.setNewPassword(
                    setNewPasswordData,
                    (err: any, response: any) => {

                        if (err) {
                            next(new error.GeneralError('Setting new password failed'));
                        } else if (response) {
                            next(
                                new GeneralResponse(
                                    'Password resetting successful',
                                    undefined,
                                    config.SUCCESS
                                )
                            );
                        } else {
                            next(new error.GeneralError('Resetting password failed'));
                        }
                    });
            } catch (err) {
                next(new error.GeneralError('error while resetting password'));
            }
        } else {
            next(new error.UnAuthorized('enter correct details'));
        }
    };  

}

export default PasswordController;