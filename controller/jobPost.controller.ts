import { NextFunction, Request, Response } from 'express';
import JobPostService from '../services/jobPost.service';
import { CreatedResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';
import { CreateJobPostDto } from '../dtos/jobPost.dto';

class JobPostController {
    public jobPostService = new JobPostService();

    public postJob = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
                const jobPostData: CreateJobPostDto = req.body;
                await this.jobPostService.create(jobPostData, (err: any, response: object) => {
                    if (err) {
                        console.log(err);
                        
                        next(new error.GeneralError('adding Job failed'));
                    } else {
                        next(new CreatedResponse('Job successfully added', undefined, configg.HTTP_CREATED));
                    }
                });
            
        } catch (err) {
            console.log(err);
            
            next(new error.GeneralError('adding job failed'));
        }
    }
}

export default JobPostController; 
