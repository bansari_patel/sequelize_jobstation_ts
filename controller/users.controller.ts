import jwt from 'jsonwebtoken';
import config from 'config';
import { NextFunction, Request, Response } from 'express';
import UserService from '../services/users.service';
import { GeneralResponse , CreatedResponse } from '../utils/response.js';
import { User } from '../interface/users.interface';
import configg from '../utils/config.js';
import bcrypt from 'bcrypt';
import error from '../utils/error';
import { CreateUserDto } from '../dtos/users.dto';
import { CreateCompanyDto } from '../dtos/companyDetails.dto';
import { CreatePersonalDto } from '../dtos/personalDetails.dto';
import { CreateOtherDto } from '../dtos/otherDetails.dto';
import { CreateEducationDto } from '../dtos/educationDetails.dto';
import { CreateLocationDto } from '../dtos/locationDetails.dto';
import { CreateReferenceDto } from '../dtos/referenceDetails.dto';
import { CreateSalaryDto } from '../dtos/salaryDetails.dto';
import { CreateDocumentsDto } from '../dtos/upload_documents.dto';
import { sendConfirmationMail } from '../helper/mail.helper';
const hashRound = 10;
const getError = 'Error while getting user';

class UserController {
    public userService = new UserService();

    public registerUser = async (req: Request, res: Response, next: NextFunction) => {
        try {

            req.body.password = await bcrypt.hash(req.body.password, hashRound);
            const userData: CreateUserDto = req.body;

            await this.userService.create(userData, (err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('user registration failed'));
                } else {
                    sendConfirmationMail(response, (err: any, response: any) => {
                        if (err && !response) {
                            next(new error.GeneralError('Sending mail failed.'));
                        }else{
                            next(new CreatedResponse('user successfully registered', undefined, configg.HTTP_CREATED));
                        }
                    }
                    );
                }
            });
        } catch (err) {
            next(new error.GeneralError('user registration failed'));
        }
    }
    public getUserByEmail = async (req: Request, res: Response, next: NextFunction) => {
        try {

            const email: string = req.params.email_id;

            await this.userService.isUserExist(email, (err: any, response: User[]) => {

                if (typeof response != 'undefined' && response === null) {
                    next(new error.NotFound('no user found'));
                } else if (err) {
                    next(new error.GeneralError('Error while getting user'));
                } else {
                    next(new GeneralResponse('User details found', response));
                }
            });



        } catch (err) {
            next(new error.GeneralError(getError));
        }
    }

    public updateCompanyDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const companyData: CreateCompanyDto = req.body;
            const username: any = req.decoded;

            await this.userService.updateCompany(companyData, username.email_id, (err: any, response: object) => {

                if (err || response == null) {
                    next(new error.GeneralError('Company details updation failed'));
                } else {

                    next(new GeneralResponse('Company details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Company details updation failed'));
        }
    }

    public updatePersonalDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const personalData: CreatePersonalDto = req.body;
            const username: any = req.decoded;
            await this.userService.updatePersonal(personalData, username.email_id, (err: any, response: object) => {
                if (err || response == null) {
                    next(new error.GeneralError('Personal details updation failed'));
                } else {
                    next(new GeneralResponse('Personal details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Personal details updation failed'));
        }
    }

    public updateOtherDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const otherData: CreateOtherDto = req.body;
            const username: any = req.decoded;
            await this.userService.updateOther(otherData, username.email_id, (err: any, response: object) => {

                if (err || response == null) {
                    next(new error.GeneralError('Other details updation failed'));
                } else {
                    next(new GeneralResponse('Other details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Other details updation failed'));
        }
    }

    public updateEducationDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const educationData: CreateEducationDto = req.body;
            const username: any = req.decoded;
            await this.userService.updateEducation(educationData, username.email_id, (err: any, response: object) => {
                if (err || response == null) {
                    next(new error.GeneralError('Education details updation failed'));
                } else {
                    next(new GeneralResponse('Education details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Education details updation failed'));
        }
    }

    public updateLocationDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const locationData: CreateLocationDto = req.body;
            const username: any = req.decoded;
            await this.userService.updateLocation(locationData, username.email_id, (err: any, response: object) => {
                if (err || response == null) {
                    next(new error.GeneralError('Location details updation failed'));
                } else {
                    next(new GeneralResponse('Location details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Location details updation failed'));
        }
    }

    public updateReferenceDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const referenceData: CreateReferenceDto = req.body;
            const username: any = req.decoded;
            await this.userService.updateReference(referenceData, username.email_id, (err: any, response: object) => {
                if (err || response == null) {
                    next(new error.GeneralError('Reference details updation failed'));
                } else {
                    next(new GeneralResponse('Reference details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Reference details updation failed'));
        }
    }

    public updateSalaryDetails = async (req: any, res: Response, next: NextFunction) => {
        try {

            const salaryData: CreateSalaryDto = req.body;
            const username: any = req.decoded;
            await this.userService.updateSalary(salaryData, username.email_id, (err: any, response: object) => {
                if (err || response == null) {
                    next(new error.GeneralError('Salary details updation failed'));
                } else {
                    next(new GeneralResponse('Salary details successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Salary details updation failed'));
        }
    }

    public uploadDocuments = async (req: any, res: Response, next: NextFunction) => {
        try {

            const documentsDetails: CreateDocumentsDto = req.body;


            const fileData = JSON.parse(JSON.stringify(req.files));
            documentsDetails.upload_photo = fileData.upload_photo[0].filename;
            documentsDetails.upload_resume = fileData.upload_resume[0].filename;

            const username: any = req.decoded;
            await this.userService.uploadDocuments(documentsDetails, username.email_id, (err: any, response: object) => {

                if (err || response == null) {
                    next(new error.GeneralError('Documents updation failed'));
                } else {
                    next(new GeneralResponse('Documents successfully updated', undefined, configg.HTTP_SUCCESS));
                }
            });

        } catch (err) {
            next(new error.GeneralError('Documents updation failed'));
        }
    }
}

export default UserController;
 