import { NextFunction, Request, Response } from 'express';
import GroupTitleService from '../services/groupTitle.service';
import { GeneralResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';

class GroupTitleController {
    public groupTitleService = new GroupTitleService();

    public getGroupTitleData = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.groupTitleService.getGroupTitleData((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching Group Title data failed'));
                } else {
                    next(new GeneralResponse('Group Title data successfully fetched', response, configg.HTTP_SUCCESS));
                }
            });
        } catch (err) {
            next(new error.GeneralError('fetching Group Title data failed'));
        }
    }

    public getGroupTitles = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.groupTitleService.getGroupTitles((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching Group Titles failed'));
                } else {
                    next(new GeneralResponse('Group Titles successfully fetched', response, configg.HTTP_SUCCESS));
                }
            });
        } catch (err) {
            next(new error.GeneralError('fetching Group Titles failed'));
        }
    }
}

export default GroupTitleController; 
