import { NextFunction, Request, Response } from 'express';
import AppliedJobService from '../services/appliedJob.service';
import { CreatedResponse} from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';
import { CreateAppliedJobDto } from '../dtos/appliedJob.dto';

class AppliedJobController {
    public appliedJobservice = new AppliedJobService();

    public addAppliedJob = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
                const appliedJobData: CreateAppliedJobDto = req.body;
                await this.appliedJobservice.create(appliedJobData, (err: any, response: object) => {
                    if (err) {
                        next(new error.GeneralError('Job application failed'));
                    } else {
                        next(new CreatedResponse('Job successfully applied', undefined, configg.HTTP_CREATED));
                    }
                });
            
        } catch (err) {
            next(new error.GeneralError('Job application failed'));
        }
    }
}

export default AppliedJobController; 
