import { NextFunction, Request, Response } from 'express';
import UserService from '../services/users.service';
import LoginService from '../services/login.service';
import { GeneralResponse } from '../utils/response.js';
import bcrypt from 'bcrypt';
import error from '../utils/error';
import { generateAuthToken } from '../helper/login.helper';
const hashRound = 10;

class LoginController {
    public userService = new UserService();
    public loginService = new LoginService();

    public userLogin = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const hashPassword = await bcrypt.hash(req.body.password, hashRound);
            await this.userService.isUserExist(req.body.username, async (err: any, response: any) => {

                if (err || response == null) {
                    next(new error.UnAuthorized('Invalid username or Password'));
                } else {
                    bcrypt.compare(req.body.password, response.password, (ERR, RES) => {
                        if (ERR) {
                            next(new error.UnAuthorized('Invalid username or Password'));
                        } else if(RES) {
    
                            const token = generateAuthToken(response);
    
                            req.body.password = hashPassword;
    
                            this.loginService.create(req.body, (errAuth: any, responseAuth: any) => {
                                if (errAuth == null) {
    
                                    const loginResponse = {
                                        mobile: response.mobile,
                                        email_id: response.email_id,
                                        name: response.name,
                                        surname: response.surname,
                                        current_company: response.current_company,
                                        designation: response.designation,
                                        education: response.education,
                                        experience: response.experience,
                                        current_salary: response.current_salary,
                                        expected_salary: response.expected_salary,
                                        notice_period: response.notice_period,
                                        date_of_birth: response.date_of_birth,
                                        current_location: response.current_location,
                                        home_town: response.home_town,
                                        preferred_location: response.preferred_location,
                                        reference_person: response.reference_person,
                                        apply_post: response.apply_post,
                                        resume: response.upload_resume,
                                        photo: response.upload_photo,
                                        auth_token: token
                                    }
    
                                    next(new GeneralResponse('Authorized user', loginResponse));
                                } else {
                                    next(new error.GeneralError('User Login failed'));
                                }
                            })
    
                        }else{
                            next(new error.UnAuthorized('Invalid username or Password'));
                        }
                    });
                    
                }
            });
        } catch (err) {
            next(new error.GeneralError('User Login failed.'));
        }
    }

}

export default LoginController; 