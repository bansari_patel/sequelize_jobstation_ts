import { NextFunction, Request, Response } from 'express';
import RoleDataService from '../services/roleData.service';
import { GeneralResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';

class RoleDataController {
    public roleDataService = new RoleDataService();

    public getRoleData = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.roleDataService.getRoleData((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching role data failed'));
                } else {
                    next(new GeneralResponse('role data successfully fetched', response, configg.HTTP_SUCCESS));
                }
            });
        } catch (err) {
            next(new error.GeneralError('fetching role data failed'));
        }
    }

    public getRoles = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.roleDataService.getRoles((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching roles failed'));
                } else {
                    next(new GeneralResponse('roles successfully fetched', response, configg.HTTP_SUCCESS));
                }
            });
        } catch (err) {
            next(new error.GeneralError('fetching roles failed'));
        }
    }
}

export default RoleDataController; 
