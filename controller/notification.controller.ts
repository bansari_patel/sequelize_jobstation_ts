import { NextFunction, Request, Response } from 'express';
import NotificationService from '../services/notification.service';
import { GeneralResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';

class NotificationController {
    public notificationService = new NotificationService();

    public getNotificationData = async (req: Request, res: Response, next: NextFunction) => {
        try {
                const applied_id = Number(req.params.applied_id);
            await this.notificationService.getDataByAppliedId(applied_id, (err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching notification data failed'));
                } else {
                    next(new GeneralResponse('notification data successfully fetched', undefined, configg.HTTP_SUCCESS));
                }
            });
            
        } catch (err) {
            next(new error.GeneralError('fetching notification data failed'));
        }
    }
}

export default NotificationController; 
