import { NextFunction, Request, Response } from 'express';
import IndustryFunctionService from '../services/industryFunction.service.js';
import { GeneralResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';

class IndustryFunctionController {
    public industryFunctionService = new IndustryFunctionService();

public getIndustryFunctionData = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.industryFunctionService.getIndustryFunctionData((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching Industry Functions data failed'));
                } else {
                    next(new GeneralResponse('Industry Functions data successfully fetched', response, configg.HTTP_SUCCESS));
                }
            });
        } catch (err) {
            next(new error.GeneralError('fetching Industry Functions data failed'));
        }
    }

    public getIndustyFunctions = async (req: Request, res: Response, next: NextFunction) => {
        try {

            await this.industryFunctionService.getIndustryFunctions((err: any, response: object) => {

                if (err) {
                    next(new error.GeneralError('fetching Industry Functions failed'));
                } else {
                    next(new GeneralResponse('Industry Functions successfully fetched', response, configg.HTTP_SUCCESS));
                }
            }); 
        } catch (err) {
            next(new error.GeneralError('fetching Industry Functions failed'));
        }
    }
}

export default IndustryFunctionController;
