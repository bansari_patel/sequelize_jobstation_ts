import { NextFunction, Request, Response } from 'express';
import JobExperienceService from '../services/jobExperience.service';
import { GeneralResponse , CreatedResponse } from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';
import { CreateJobExperienceDto } from '../dtos/jobExperience.dto';

class JobExperienceController {
    public jobExperienceService = new JobExperienceService();

    public addExperienceData = async (req: Request, res: Response, next: NextFunction) => {
        try {
           
                const jobExperienceData: CreateJobExperienceDto = req.body;

                await this.jobExperienceService.create(jobExperienceData, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('adding Job experience failed'));
                    } else {
                        next(new CreatedResponse('Job experience successfully added', undefined, configg.HTTP_CREATED));
                    }
                });
            
        } catch (err) {
            next(new error.GeneralError('adding job experience failed'));
        }
    }
    public getExperienceData = async (req: Request, res: Response, next: NextFunction) => {
        try {
           
                    const candidate_id = Number(req.params.candidate_id);
                await this.jobExperienceService.getDataByCandidateId(candidate_id, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('fetching job experience data failed'));
                    } else {
                        next(new GeneralResponse('Job experience data successfully fetched', response, configg.HTTP_SUCCESS));
                    }
                });
            
        } catch (err) {
            next(new error.GeneralError('fetching job experience data failed'));
        }
    }

    public removeJobExperienceData = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
                const candidate_id = Number(req.params.candidate_id);

                await this.jobExperienceService.delete(candidate_id, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('removing Job Experience Data failed'));
                    } else {
                        next(new GeneralResponse('Job Experience Data successfully removed', undefined, configg.HTTP_SUCCESS));
                    }
                }); 
            
        } catch (err) {
            next(new error.GeneralError('removing Job Experience Data failed'));
        }
    }
}

export default JobExperienceController;
