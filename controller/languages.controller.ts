
import { NextFunction, Request, Response } from 'express';
import LanguageService from '../services/languages.service';
import { GeneralResponse , CreatedResponse} from '../utils/response.js';
import configg from '../utils/config.js';
import error from '../utils/error';
import { CreateLanguageDto } from '../dtos/languages.dto';
import { IntegerDataType } from 'sequelize/types';

class LanguageController {
    public languageService = new LanguageService();

    public addLanguage = async (req: Request, res: Response, next: NextFunction) => {
        try {

                const languageData: CreateLanguageDto = req.body;

                await this.languageService.create(languageData, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('adding language failed'));
                    } else {
                        next(new CreatedResponse('language successfully added', undefined, configg.HTTP_CREATED));
                    }
                });

        } catch (err) {
            next(new error.GeneralError('adding language failed'));
        }
    }

    public updateLanguage = async (req: Request, res: Response, next: NextFunction) => {
        try {

                const language_id = Number(req.params.id);
                const languageData: CreateLanguageDto = req.body;

                await this.languageService.update(languageData, language_id, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('updating language failed'));
                    } else {
                        next(new GeneralResponse('language successfully updated', undefined, configg.HTTP_CREATED));
                    }
                });
            
        } catch (err) {
            next(new error.GeneralError('updating language failed'));
        }
    }

    public getLanguage = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
                const candidate_id = Number(req.params.candidate_id);
                await this.languageService.getLanguagesByCandidateId(candidate_id, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('fetching language failed'));
                    } else {
                        next(new GeneralResponse('language successfully fetched', response, configg.HTTP_SUCCESS));
                    }
                });
            
        } catch (err) {
            next(new error.GeneralError('fetching language failed'));
        }
    }

    public removeLanguage = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
                const language_id = Number(req.params.id);

                await this.languageService.delete(language_id, (err: any, response: object) => {

                    if (err) {
                        next(new error.GeneralError('removing language failed'));
                    } else {
                        next(new GeneralResponse('language successfully removed', undefined, configg.HTTP_SUCCESS));
                    }
                }); 
            
        } catch (err) {
            next(new error.GeneralError('removing language failed'));
        }
    }
}

export default LanguageController;
