import config from 'config';
import Sequelize from 'sequelize';
import { DbConfig } from '../interface/db.interface';
import UserModel from '../models/users.model';
import AuthModel from '../models/login.model';
import LanguageModel from '../models/languages.model';
import AppliedJobModel from '../models/appliedJob.model';
import JobExperienceModel from '../models/jobExperience.model';
import NotificationModel from '../models/notification.model';
import JobPostModel from '../models/jobPost.model';
import RoleDataModel from '../models/roleData.model';
import GroupTitleModel from '../models/groupTitle.model';
import industryFunctionModel from '../models/industryFunction.model';

const { host, user, password, database, pool }: DbConfig = config.get('dbConfig');

const sequelize = new Sequelize.Sequelize(database, user, password, {
    host: host,
    dialect: 'mysql',
    timezone: '+09:00',
    define: {
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci', 
        underscored: true,
        freezeTableName: true,
        timestamps: false,
        createdAt: false,
        updatedAt: false
    },
    pool: {
        min: pool.min,
        max: pool.max,
    },
    logging: false,
    benchmark: true,
});

async function checkConnection(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    }catch (error){
        console.error('Unable to connect to the database:', error);
    }
    
    await sequelize.sync();
    console.log("All models were synchronized successfully.");
}
checkConnection();


const DB = {
  
  sequelize,
  Sequelize,
  Users: UserModel(sequelize),
  Login: AuthModel(sequelize),
  Language: LanguageModel(sequelize),
  AppliedJob: AppliedJobModel(sequelize),
  JobExperience: JobExperienceModel(sequelize),
  Notification: NotificationModel(sequelize),
  JobPost: JobPostModel(sequelize),
  RoleData: RoleDataModel(sequelize),
  GroupTitle: GroupTitleModel(sequelize),
  IndustryFunction: industryFunctionModel(sequelize)
};

export default DB;
